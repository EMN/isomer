# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "isomer/version"

Gem::Specification.new do |s|
  s.name        = "isomer"
  s.version     = Isomer::VERSION
  s.authors     = ["Che Ruisi-Besares"]
  s.email       = ["che@evolvingmedia.org"]
  s.homepage    = "http://www.evolvingmedia.net"
  s.summary     = %q{Sane way to handle complex deploys.}
  s.description = %q{Sets up dry deploy scripts.}

  s.rubyforge_project = "isomer"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
  s.add_dependency "rails"
  s.add_dependency "capistrano", '~>2.13.4'
  s.add_dependency "rvm-capistrano"
  s.add_dependency "capistrano-ext"
  s.add_dependency "rake"
  s.add_dependency "unicorn"
end

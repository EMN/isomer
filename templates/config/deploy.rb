require 'capistrano/isomer'

# These are the default routines that are required on inital installation
before "deploy:cold" do
	default_run_options[:pty]=true # Allow user to answer system prompts for authorized keys etc
	setup.ensure_rvmrc             # Ensure that there is a rvmrc file
	rvm.install_ruby               # Install the correct version of ruby specified config
	setup.setup_dir                # Create the skeleton /srv/$project/shared/{log,system,pids,config,init}
	setup.ensure_bundler           # Bundle install
end
	
after "deploy:cold" do
	nginx.config.rebuild_server    # Set up nginx configuration
	unicorn.config.build_config    # Build the unicorn configuration
	unicorn.start                  # Start unicorn for the first time
end
	
after "deploy:restart" do
	unicorn.restart                # Hook unicorn in after the restart script
end
	
before "deploy:restart" do
	if respond_to?(:precompile_assets) && precompile_assets != 'false'
		assets.precompile          # Compile the assests if needed by the config file
	end
end

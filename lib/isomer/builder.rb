require "rake"
require "yaml"

module Isomer
  module Build
    module Deploy

      def self.init
        populate_deploy
        capify
      end

      protected
      
      def self.capify
        system %(capify .)
      end

      def self.populate_deploy
        template_dir = File.dirname(__FILE__).split("/")[0...-2].join("/") + "/templates/"
        FileList["#{template_dir}**/**"].exclude("#{template_dir}**/**.*").each do |dir|
          if Dir.exists?(dir.slice((template_dir.length)..-1))
            puts "[skip] \"#{dir.slice((template_dir.length)..-1)}\" already exists"
          else
            Dir.mkdir(dir.slice((template_dir.length)..-1))
            puts "[created] \"#{dir.slice((template_dir.length)..-1)}\""
          end
        end
        ( FileList["#{template_dir}**/**"] - FileList["#{template_dir}**/**"].exclude("#{template_dir}**/**.*") ).each do |file|
          if File.exists? file.slice((template_dir.length)..-1)
            puts "[skip] \"#{file.slice((template_dir.length)..-1)}\" already exists"
          else
            FileUtils.cp file, file.slice((template_dir.length)..-1)
            puts "[created] \"#{file.slice((template_dir.length)..-1)}\""
          end
        end
      end
    end

    module Box

      def self.init
        install
      end

      def self.install
        system %( vagrant init )
      end
    end

    module Project

      def self.rails_project(name)
        install(name)
      end

      def self.install(name)
        system %(rails new #{name})
      end
    end
  end
end

module Capistrano
  class ConfigBuilderRunner
    IGNORED_CAPISTRANO_VARIABLES = %w(password logger releases source latest_revision).freeze

    def initialize(context, template, user_variables)
      @context, @template, @user_variables = context, template, user_variables
      @options = extract_options!(@user_variables)
    end
    
    def template
      "--template #{escape(@template)}"
    end
    
    def options
      @options.map { |key,value| "--#{key} #{escape(value)}" }.join(' ')
    end
    
    def capistrano_variables
      variables = @context.variables.reject { |key,value| IGNORED_CAPISTRANO_VARIABLES.include?(key.to_s) }
      variables.map { |key,value| "#{key}=#{format(value)}" }
    end
    
    def variables
      variables = capistrano_variables + @user_variables
      variables.map! { |variable| escape(variable) }.join(' ')
    end
    
    def run
      bundle_cmd     = @context.fetch(:bundle_cmd, "bundle")
      app_path = @context.fetch(:latest_release)
      if app_path.to_s.empty?
        raise error_type.new("Cannot detect current release path - make sure you have deployed at least once.")
      end
      @context.run "cd #{app_path} && #{bundle_cmd} exec isomer #{template} #{options} #{variables}"
    end
    
    # def builder_exec
    #   if @context.respond_to?(:builder_exec)
    #     @context.builder_exec
    #   else
    #     raise NoMethodError, 'You must set builder_exec in your Capistrano deploy receipe.'
    #   end
    # end
    
    private
      def extract_options!(args)
        args.last.is_a?(Hash) ? args.pop : {}
      end
      
      def format(value)
        value = value.call if value.respond_to?(:call)
        value = value.join(' ') if value.respond_to?(:join)
        value.to_s
      end
      
      def escape(value)
        %("#{value.to_s.gsub('"', '\"')}")
      end
  end
end

def build_config_for(template, *args)
  Capistrano::ConfigBuilderRunner.new(self, template, args).run
end

require 'erb'

unless defined?(require_relative)
  def require_relative(path)
    path = File.join(File.dirname(__FILE__), path)
    path = File.expand_path(path)    
    require(path)
  end
end

require_relative 'config_builder/assignments'
require_relative 'config_builder/template'
require_relative 'config_builder/builder'
module ConfigBuilder
  class Assignments < Hash
    def self.normalize(key)
      key.to_s.downcase
    end
    
    def initialize(assignments = {})
      update(assignments)
    end
    
    def []=(key, value)
      return if key.nil? || key.empty?
      super(self.class.normalize(key), value)
    end
    
    def [](key)
      super(self.class.normalize(key))
    end
    
    def update(hash)
      hash.each { |key,value| self[key] = value }
    end
    
    def to_binding(object = Object.new)
      object.instance_eval("def prepare_bindings(#{keys.join(', ')}) binding end")
      object.prepare_bindings(*values)
    end
  end
end

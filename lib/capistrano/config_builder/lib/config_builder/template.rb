module ConfigBuilder
  class Template < ERB
    def initialize(filename)
      @template = ERB.new(File.read(filename))
    end
    
    def interpolate(assignments)
      @template.result(assignments.to_binding)
    end
  end
end

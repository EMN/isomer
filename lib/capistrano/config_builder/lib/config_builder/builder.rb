module ConfigBuilder
  class Builder
    attr_accessor :template, :output
    
    def initialize(template = nil, output = nil)
      @template, @output = template, output || STDOUT
      @assignments = Assignments.new
    end
    
    def assign(variables)      
      case variables
      when Array
        variables.each { |assignment| assign(assignment) }
      when String
        key, value = variables.split('=')
        @assignments[key] = value
      else
        # ENV acts like a Hash, but does report itself as a Hash. Deal with it in the catch-all.
        @assignments.update(variables)
      end
    end
    
    def render
      open { |io| io.puts(Template.new(@template).interpolate(@assignments)) }
    end
    
    protected
      def open(&block)
        case @output
        when IO; yield @output
        when String; File.open(@output, 'w', &block)
        end
      end
  end
end

Capistrano::Configuration.instance.load do
	namespace :assets do
	    desc "Precompile Rails asset pipeline assets"
	    task :precompile do
	      run %(cd "#{deploy_to}/current" && RAILS_ENV=#{rails_env} bundle exec rake assets:precompile )
	    end
	end
end

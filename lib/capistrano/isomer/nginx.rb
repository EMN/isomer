Capistrano::Configuration.instance.load do     
  namespace :nginx do

    _cset :nginx_init, "/etc/init.d/nginx"

    task :start do
      run %( sudo #{nginx_init} start )
    end

    task :stop do
      run %( sudo #{nginx_init} stop )
    end

    task :restart do
      run %( sudo #{nginx_init} restart )
    end

    namespace :config do
      desc "rebuild or build the nginx configuration for the enviornment"
      task :rebuild_server do
        _cset :nginx_host_name, defined?(servername)? servername : app_short_name
        if remote_file_exists? "#{current_path}/config/nginx/#{rails_env}.conf.erb"
          set :nginx_config_target, "#{current_path}/config/nginx/#{rails_env}.conf.erb"
        else
          set :nginx_config_target, "#{current_path}/config/nginx/default.conf.erb"
        end
        build_config_for nginx_config_target, :output => "#{shared_path}/config/#{app_short_name}-nginx-#{rails_env}.conf"
      end
    end
  end
end
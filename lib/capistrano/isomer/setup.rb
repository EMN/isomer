Capistrano::Configuration.instance.load do
  namespace :setup do
    require 'capistrano/cli'

    desc "install system init script"
    task :install_init_scritps do
      puts "TODO add the system init script here"
    end

    desc "Set up server"
    task :prepserver do
      install_init_scritps
      set(:setup_user) do
        Capistrano::CLI.ui.ask "What user do you want to set this server with?"
      end
      set :user_to_setup, user
      set :user, setup_user
      if capture( %( id #{user_to_setup} > /dev/null 2>&1 && echo true || echo false ), :shell => "#{rvm_install_shell}" ).strip == "true"
        set :continue_with_install_check, "N"
        set(:continue_with_install_check) do
          Capistrano::CLI.ui.ask "The users exists do you want to continue? [y/N]"
        end
      else
        run " #{sudo} useradd -G #{deployer_groups} -s /bin/bash -m #{user_to_setup}", :shell => "#{rvm_install_shell}", :pty => true
      end
      _cset :continue_with_install_check,"y"
      if continue_with_install_check == "y"
        run "#{sudo} mkdir -p /home/#{user_to_setup}/.ssh/", :shell => "#{rvm_install_shell}", :pty => true
        run "#{sudo} chown #{user_to_setup}:#{user_to_setup} /home/#{user_to_setup}/.ssh/", :shell => "#{rvm_install_shell}", :pty => true
        run "#{sudo} [ ! -f /home/#{user_to_setup}/.ssh/id_rsa ] && #{sudo} su - #{user_to_setup} -c \"ssh-keygen -b 4096 -t rsa -f /home/#{user_to_setup}/.ssh/id_rsa -N '' -q\" || echo ''", :shell => "#{rvm_install_shell}", :pty => true
        #would have done this with capture but it was broken for sudo commands even after that patch and update
        run "#{sudo} cat /home/#{user_to_setup}/.ssh/id_rsa.pub", :shell => "#{rvm_install_shell}", :pty => true do |channel, stream, data|
          if data != "" and ! data.nil?
            set :generated_ssh_key, "#{data}"
          end
        end
        run "cat /dev/null | #{sudo} tee /home/#{user_to_setup}/.ssh/authorized_keys", :shell => "#{rvm_install_shell}", :pty => true
        authorized_keys.split("\n").each do |key|
          run "echo -e #{key} | #{sudo} tee -a /home/#{user_to_setup}/.ssh/authorized_keys", :shell => "#{rvm_install_shell}", :pty => true
        end
        run "#{sudo} chown #{user_to_setup}:#{user_to_setup} /home/#{user_to_setup}/.ssh/authorized_keys", :shell => "#{rvm_install_shell}", :pty => true
        puts "The generated depoly key is: "
        puts "#{generated_ssh_key}"
        puts "Please put this in the git-lab deploy keys section for this project to allow deploys"
      else
        abort "Aborting user setup..."
      end
    end
    
    desc "Set up the needed dir structure"
    task :setup_dir do
      run "mkdir -p #{shared_path}/{log,system,config,init}", :shell => "#{rvm_install_shell}"
      deploy.setup
    end

    desc "ensure bundler is installed"
    task :ensure_bundler do
      run "type -P bundle &>/dev/null || gem install bundler--no-rdoc --no-ri", :shell => "#{default_shell}"
    end
    
    desc "Update the rvmrc file"
    task :update_rvmrc do
      #depricated
    end

    desc "Ensure rvmrc file"
    task :ensure_rvmrc do
      abort "Aborting you don't have a .rvmrc file or .ruby_version files in the project dir." unless ( File.join(Dir.getwd, ".ruby_version") or File.join(Dir.getwd, ".rvmrc"))
    end

    desc "Trust rvmrc file"
    task :trust_rvmrc do
      #depricated
    end
  end
end
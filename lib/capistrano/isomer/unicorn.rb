Capistrano::Configuration.instance.load do
  namespace :unicorn do

    _cset :unicorn_init_script_location, "#{shared_path}/init/#{app_short_name}"
    _cset :unicorn_configuration_location, "#{shared_path}/config/#{rails_env}.rb"

    task :ensure_required do
      config.build_init_script unless remote_file_exists? unicorn_init_script_location
      config.build_config unless remote_file_exists? unicorn_configuration_location
    end

    desc "restart Uincorn master process"
    task :restart do
      ensure_required
      run %( #{unicorn_init_script_location} restart )
    end

    desc 'Start Unicorn master process'
    task :start, :roles => :app, :except => {:no_release => true} do
      ensure_required
      run %( #{unicorn_init_script_location} start )
    end

    desc 'Stop Unicorn'
    task :stop do
      ensure_required
      run %( #{unicorn_init_script_location} stop )
    end

    desc "Reload Unicorn"
    task :reload do
      ensure_required
      run %( #{unicorn_init_script_location} reload )
    end

    namespace :config do
      task :build_init_script do
        if remote_file_exists? "#{current_path}/config/unicorn/init.d/#{rails_env}.erb"
          set :unicorn_config_target, "#{current_path}/config/unicorn/init/#{rails_env}.erb"
        else
          set :unicorn_config_target, "#{current_path}/config/unicorn/init/default.erb"
        end
        build_config_for unicorn_config_target, :output => "#{unicorn_init_script_location}"
        run "chmod +x #{unicorn_init_script_location}"
      end
      task :build_config do
        if remote_file_exists? "#{current_path}/config/unicorn/config/#{rails_env}.rb.erb"
          set :unicorn_config_target, "#{current_path}/config/unicorn/config/#{rails_env}.rb.erb"
        else
          set :unicorn_config_target, "#{current_path}/config/unicorn/config/default.rb.erb"
        end
        build_config_for unicorn_config_target, :output => "#{unicorn_configuration_location}"
      end
    end
  end
end
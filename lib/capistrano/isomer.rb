require 'fileutils'
Capistrano::Configuration.instance.load do
	begin
		require 'rails'
		_cset :yaml_file, "#{Rails.root}config/deploy.yml"
	rescue LoadError
		_cset :yaml_file, "#{`pwd`.strip}/config/deploy.yml"
	end

	Capistrano::Configuration::Namespaces::Namespace.class_eval do
	  def capture(*args)
	    parent.capture *args
  	  end
 	  def remote_file_exists?(path)
  		results = []

  		invoke_command("if [ -e '#{path}' ]; then echo -n 'true'; fi") do |ch, stream, out|
    		results << (out == 'true')
  		end
  		return false unless ! results.empty?
  		results.all?
	  end
	end

	def synthesize!
		debug = true
		puts "sythesize start" if debug
		location = fetch(:stage_dir, "config/deploy")
		yaml = YAML.load_file(yaml_file)
		_cset :stages, yaml.keys
		yaml.each_pair do |k,v|
			puts "task #{k} created" if debug
    		desc "Set the target stage to `#{k}'."
    		task(k) do
    			set :stage, k.to_sym
    			load "#{location}/#{stage}" if File.exists? "#{location}/#{stage}.rb" 
    		end
    	end

		yaml[ARGV.first].each_pair do |k,v|
		  	if k == "roles"
		  		v.each_pair do |r,s|
		  			puts "role #{r.to_sym}, #{s}" if debug
		  			if s =~ /^[\'\"]/
		  				instance_eval "role :#{r.to_sym}, #{s}"
		  			else
		  				role r.to_sym, s
		  			end
		  		end
		  	else
		  		v.to_s.scan(/\#\{[^\}]*\}/).each do |ruby|
		  			v = v.gsub(ruby, eval(ruby.gsub(/\#\{/, "").gsub(/\}/, "")))
		  		end
		  		puts "set #{k.to_sym}, #{v}" if debug
		  		set k.to_sym, v
		  	end
		end

		if defined?(:all_enviornments)
			set :rake_env, all_enviornments
			set :rails_env, all_enviornments
			set :unicorn, all_enviornments
		end

		

		before "deploy:cold" do
			if deploy_via == "remote_cache"
				set :deploy_via, :checkout
			end
		end

		require 'rvm/capistrano'
		require 'bundler/capistrano'
		require 'capistrano/config_builder/lib/capistrano/config_builder'
		require 'capistrano/isomer/setup'
		require 'capistrano/isomer/unicorn'
		require 'capistrano/isomer/nginx'
		require 'capistrano/isomer/assets'

		on :load do
			unless roles.keys.include? "db".to_sym
				namespace :deploy do ; task :migrate do ; end ; end
			end
			isomer.loaded
		end

	end

	synthesize!
	
	namespace :isomer do

		task :loaded do
			if stages.include?(ARGV.first)
      			# Execute the specified stage so that recipes required in stage can contribute to task list
      			find_and_execute_task(ARGV.first) if ARGV.any?{ |option| option =~ /-T|--tasks|-e|--explain/ }
   			else
      			# Execute the default stage so that recipes required in stage can contribute tasks
      			find_and_execute_task(default_stage) if exists?(:default_stage)
    		end
		end
	end
end
